# ThreeWayHandshakeWithUDP

Implementasi dari Three Way Handshake yang diterapkan pada protokol TCP menggunakan UDP

Anggota Kelompok
1. Gunawan Wibisono     (4210171002)
2. Septian Nabilah S.   (4210171004)
3. Mayvan Gandhy        (4210171007)
4. Lanisya Febriyani    (4210171008)
5. Candra Wira M.       (4210171012)
6. Bayu Prajanata       (4210171014)
7. Titan Adi Narendra   (4210171017)
8. M. Luqman H.         (4210171019)
9. Oryza Umi S.         (4210171020)
10. Fahmi Noor A.       (4210171025)
11. Fakhriy Ramadhan N. (4210171027)
12. Alizza Salsabilah   (4210171028)
