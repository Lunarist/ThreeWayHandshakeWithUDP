import java.io.*;
import java.net.*;

class Data
{
	int stats;
	int data;
	
	public Data(int stats, int data)
	{
		this.stats = stats;
		this.data = data;
	}
}

public class Client_UDP 
{
	// Inisialisasi enum
	public enum status
	{
		SYN, SYNACK, ACK, FIN, FINACK, DATA
	}
	
	public static void main(String[] args) throws IOException
	{
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		
		// Membuat Socket untuk Client
		DatagramSocket clientSocket = new DatagramSocket();
		System.out.println("Client Socket Created");
		InetAddress IPAddress = InetAddress.getByName("localhost");
		
		byte[] sendData = new byte[1024];
		byte[] receiveData = new byte[1024];
		int port = 8080;
		
		status clientState = status.SYN;
		boolean wait = false;
		
		int count = 0;
		
		while (true)
		{
			if(clientState != status.DATA && !wait) 
			{
				byte[] send = new byte[1024];
				
				
				// Proses 3 Way Handshake
				if(clientState == status.SYN) 
				{
					System.out.println("Client send SYN");
					send = (status.SYN.ordinal() + "-" + count).getBytes();
				}
				else if(clientState == status.ACK ) 
				{
					System.out.println("Client send ACK");
					send = (status.ACK.ordinal() + "-" + count).getBytes();
					
					clientState = status.DATA;
				}
				
				DatagramPacket sendPacket = new DatagramPacket(send, send.length, IPAddress, port);
				clientSocket.send(sendPacket);
				
				if(count >= 5000)
					break;
				
				wait = true;
			}
			
			// Penerimaan 
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			clientSocket.receive(receivePacket);
			
			String sentence = new String(receivePacket.getData());
			
			String _stats = sentence.substring(0, sentence.indexOf("-"));
			String _data = sentence.substring(sentence.indexOf("-") + 1).trim();
			
			//System.out.println("Received : " + sentence);
			
			// ParseInt = merubah string menjadi int
			Data data = new Data(Integer.parseInt(_stats), Integer.parseInt(_data));
			
			// Proses 3 Way Handshake
			// .ordinal() digunakan untuk mendapatkan value dari Enum
			if(data.stats == status.SYNACK.ordinal()) 
			{
				System.out.println("Client got SYNACK");
				wait = false;
				clientState = status.ACK;
			}
			else if(data.stats == status.DATA.ordinal()) 
			{
				System.out.println("Client got DATA: " + data.data);
				byte[] send = (status.DATA.ordinal() + "-" + 10).getBytes();
				
				// Jika data yang dikirim sudah menyampai angka 5000
				if(data.data >= 5000)
				{
					send = (status.FIN.ordinal() + "-" + count).getBytes();
					System.out.println("Client send FIN");
					
					DatagramPacket sendPacket = new DatagramPacket(send, send.length, IPAddress, port);
					clientSocket.send(sendPacket);
					
					continue;
				}
					
				// Send data
				for(int i = 0; i < 5; i++) 
				{
					count += 10;
					
					System.out.println("Client send 10");
					
					DatagramPacket sendPacket = new DatagramPacket(send, send.length, IPAddress, port);
					clientSocket.send(sendPacket);
				}
			}
			else if(data.stats == status.FINACK.ordinal()) 
			{
				System.out.println("Client got FINACK");
				wait = false;
				clientState = status.ACK;
			}
			
			/*if (count % 50 == 0)
			{
				System.out.println("Data check");
				count += 1;
				
				if (count >= 5000)
				{
					System.out.println("Data 5000");
					clientSocket.close();
				}
			}*/
		}
		// Menutup socket client
		clientSocket.close();
		System.out.println("Client Socket Closed");
	}
}
